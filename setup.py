from setuptools import setup, find_packages

setup(
    name='dashboard-gitlab',
    version='0.1.0',
    description='Meltano reports and dashboards for data fetched using the GitLab API',
    packages=find_packages(),
    package_data={'reports': ['*.m5o'], 'dashboards': ['*.m5o']},
    install_requires=[],
)
